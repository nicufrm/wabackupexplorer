import path from 'path';
import { watch } from 'fs';
import { fileURLToPath } from 'url';
import logger from 'morgan';
import express from 'express';
import cookieParser from 'cookie-parser';
import indexRouter from './routes/index.js';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const restartTimeout = 30000;

let timeout;
watch(__dirname + '/../Files/WhatsApp/Databases/msgstore.db.crypt15', (event, filename) => {
  console.log(`CHANGE DETECTED -> ${filename} ${event}`);
  console.log(`setting restart timer for ${restartTimeout}ms`);
  if (timeout) clearTimeout(timeout);
  timeout = setTimeout(() => {
    console.log('Restarting server');
    process.exit(0);
  }, restartTimeout);
});

const app = express();

app.use(logger(':date[iso] :remote-addr :method :url :status'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use('/v1', indexRouter);
app.use('/fe', express.static(path.join(__dirname, '../frontend/public')));
app.use('/files/WhatsApp/Media', express.static(path.join(__dirname, '../Files/WhatsApp/Media')));
app.get('/', (req, res) => res.redirect('/fe'));

export default app;
