import Database from 'better-sqlite3';
// import { fileWrite } from 'fs';

const sqlFile = '../../Files/msgstore.db';

const db = new Database(sqlFile, { readonly: true });

class Model {
  constructor(table) {
    this.table = table;
    this.db = db;
  }

  all(columns, clause) {
    let query = `SELECT ${columns} FROM ${this.table}`;
    if (clause) query += ` ${clause}`;
    const stmt = db.prepare(query);
    // stmt.raw(true);
    return stmt.all();
  }
}

const oldPhotos = new Model(`
main.message_media
LEFT JOIN message ON message_media.message_row_id = message._id
`);

const data = oldPhotos.all(`
  file_path

`, `
WHERE message_media.chat_row_id IN (194,400,57,193,254,16,11,85,96,200)
AND timestamp < 1701385200000
AND message_type IN (1,3,9,13)
LIMIT 100000
`);

console.log(`#!/bin/bash

cd /storage/emulated/0/Android/media/com.whatsapp/Whatsapp/
`);

let i = 0;
for (const line of data) {
  const filePath = line.file_path;
  if (filePath) {
    if (i++ % 100 === 0) 
      console.log(`echo '${filePath.replaceAll('\'', '\'\\\'\'')}'`);
    console.log(`rm '${filePath.replaceAll('\'', '\'\\\'\'')}'`);
  }
}
