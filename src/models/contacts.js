import parse from 'csv-parse';
import fs from 'fs';

import { contactsFile } from '../settings.js';

class Contacts {
  constructor() {
    this.loading = true;
    this.error = null;
    this.list = [];
    this.callback = null;

    fs.readFile(contactsFile, { encoding: 'utf-8' }, (err, data) => {
      if (err) {
        this.loading = false;
        this.error = 'no-contacts-found';
        this.doCallback();
      } else {
        parse(data, {}, (parseErr, output) => {
          if (parseErr) {
            this.loading = false;
            this.error = 'contacts-parse-failed';
            this.doCallback();
          } else {
            output.shift();
            const arranged = output
              .map(line => {
                const phoneString = `${line[40]}:${line[42]}:${line[44]}:${line[46]}`.replace(/[^\d+:]/g, '');
                return {
                  name: line[0],
                  phones: phoneString
                    .split(':')
                    .map(phone => phone.replace(/^0+/, ''))
                    .map(phone => phone.replace(/[^\d]/g, ''))
                    .filter(phone => phone !== '')
                };
              })
              .filter(line => line.name && line.phones.length > 0);
            this.loading = false;
            this.list = arranged;
            this.doCallback();
          }
        });
      }
    });
  }

  doCallback() {
    if (this.callback) this.callback();
  }

  onReady(callback) {
    this.callback = callback;
    if (this.loading === false) {
      process.nextTick(this.callback);
    }
  }

  match(searchNumber) {
    let index;
    index = this.list
      .findIndex(contact => contact.phones.includes(searchNumber));
    if (index !== -1) return index;
    index = this.list
      .findIndex(contact => contact.phones.findIndex(phone => searchNumber.endsWith(phone)));
    if (index !== -1) return index;
    return null;
  }
}

export const contacts = new Contacts();
