import express from 'express';
import {
  indexPage,
  messagesPage,
  chatsPage,
  contactsPage,
} from '../controllers/index.js';

const indexRouter = express.Router();

indexRouter.get('/', indexPage);
indexRouter.get('/messages/:id/:start/:count', messagesPage);
indexRouter.get('/chats', chatsPage);
indexRouter.get('/contacts', contactsPage);

export default indexRouter;
