import Model from '../models/model.js';
import { contacts } from '../models/contacts.js';

const chatsModel = new Model(`
  chat_view
  LEFT JOIN jid ON jid._id = chat_view.jid_row_id
`);

export const chatsPage = (req, res) => {
  const data = chatsModel.all('chat_view._id, jid.raw_string, chat_view.subject, chat_view.sort_timestamp, chat_view.created_timestamp', 'ORDER BY chat_view.sort_timestamp DESC');
  contacts.onReady(() => {
    const dataWithContacts = data.map(line => {
      const newLine = line;
      if (newLine.subject === null) {
        const id = contacts.match(line.raw_string.split('@')[0]);
        if (id) {
          newLine.contact = {
            id,
            name: contacts.list[id].name,
          };
        }
      }
      return newLine;
    });
    res.status(200).json({ chats: dataWithContacts });
  });
};
