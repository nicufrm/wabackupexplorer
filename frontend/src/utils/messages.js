import { match } from './contacts';

export const processMessages = (list, contacts) => {
  const generateColor = (colorIndex) => ((colorIndex === null) ? '#777777' : `hsl(${(colorIndex * 105) % 360}, 50%, 50%)`);
  const parseUser = (user) => {
    const id = match(user, contacts);
    const name = id ? contacts[id].name : user && `+${user}`;
    const color = generateColor((user && user % 10000) || null);
    return [ name, color ];
  };
  const replaceTags = (text) => {
    if (!text) return text;
    const regex = /@(\d{7,})/g;
    return text.replaceAll(regex, (matchedText, matchedNumber) => {
      const id = match(matchedNumber, contacts);
      const name = id ? contacts[id].name : matchedNumber;
      return `@${name}`;
    });
  };

  let lastUser = null;
  let lastDate = null;

  return list.reverse().map(dbLine => {
    const newLine = dbLine;
    newLine.date = Intl.DateTimeFormat(undefined, { dateStyle: 'long' }).format(dbLine.timestamp);
    newLine.time = Intl.DateTimeFormat(undefined, { timeStyle: 'short' }).format(dbLine.timestamp);
    [ newLine.name, newLine.color ] = parseUser(dbLine.user, dbLine.contact);
    newLine.isStatus = (dbLine.from_me === 1) && (dbLine.status === 6);
    newLine.isSent = (dbLine.from_me === 1) && (dbLine.status !== 6);
    newLine.hasQuote = !!dbLine.quoted;
    newLine.text_data = replaceTags(dbLine.text_data);

    if (newLine.hasQuote) {
      // eslint-disable-next-line max-len
      [ newLine.quoted.name, newLine.quoted.color ] = parseUser(dbLine.quoted.user, dbLine.quoted.contact);
      newLine.quoted.text_data = replaceTags(dbLine.quoted.text_data);
    }

    // calculate firsts
    newLine.isFirstOnDay = (lastDate !== newLine.date);
    lastDate = newLine.date;
    if (!newLine.isStatus) {
      newLine.isFirstFromUser = (lastUser !== dbLine.user);
      lastUser = dbLine.user;
    }
    return newLine;
  });
};
