ARG NODE_VERSION=20.17.0

FROM node:${NODE_VERSION}-alpine AS BUILD_IMAGE

# ENV NODE_ENV production

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --omit=dev

# Bundle app source
COPY . .

RUN npm run febuild

FROM node:${NODE_VERSION}-alpine

# Create app directory
WORKDIR /usr/src/app

# Install wa-crypt-tools
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN apk add --update py3-pip
RUN apk add --update pipx
RUN pipx install wa-crypt-tools

# copy from build image
COPY --from=BUILD_IMAGE /usr/src/app/ ./

EXPOSE 3000

ENTRYPOINT ["/bin/sh"]
CMD ["./dockerstartup.sh"]