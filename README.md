# WhatsApp Backup Explorer

### To install:

1. Run `git clone https://gitlab.com/nicufrm/wabackupexplorer.git`
1. Run `cd wabackupexplorer`
1. Run `npm install`
1. Copy `key` file from `/data/data/com.whatsapp/files/` from the phone (root required) to `Key/` (see below how to extact it)
1. Copy the following to `Files/` :
  *  `WhatsApp` folder from the phone's storage `Android/media/com.whatsapp/`
  *  `contacts.csv` from https://contacts.google.com/ (export as Google CSV)
1. ~~Run `node decrypt.js`~~ Use https://github.com/ElDavoo/wa-crypt-tools to decrypt the the database 
  * `wadecrypt -v Key/key Files/WhatsApp/Databases/msgstore.db.crypt14 Files/msgstore.db`
  or
  * `wadecrypt -v Key/encrypted_backup.key Files/WhatsApp/Databases/msgstore.db.crypt15 Files/msgstore.db`
1. Run `npm run dev`
1. Go to http://localhost:3000/fe/

### How to extract key

#### Option 1: Generate from E2E encription key

1. run `wacreatekey -v --hex ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff`

#### Option 2: Android Studio

1. Install andoid studio
1. Create an emulator with play store
      * make sure it has plenty storage
      * (last tried pixel6a with tiramisu)
1. Start emulator
1. Check if `adb shell` is working
1. Get rootAVD from https://gitlab.com/newbit/rootAVD
      * run `./rootAVD.sh ListAllAVDs`
      * run `./rootAVD.sh system-images/android-33/google_apis_playstore/arm64-v8a/ramdisk.img`
      * (don't save state on restart)
1. Cold boot after shutdown (3 dot menu)
1. Check root
      * run `adb shell`
      * run `su`
      * allow from dialog 
      * run `whoami` (shoud show root)
1. Install whatsapp
      * restore backup
1. Copy key
      * run `adb shell`
      * run `su`
      * run `cp /data/data/com.whatsapp/files/key /storage/emulated/0/Download/`
      * run `cp /data/data/com.whatsapp/files/encrypted_backup.key /storage/emulated/0/Download/`
      * run `exit`
      * run `exit` again to disconnect
      * run `adb pull /storage/emulated/0/Download/key`
      * run `adb pull /storage/emulated/0/Download/encrypted_backup.key`
      * key is in current directory now
1. Stop emulator
1. (log back in to phone now)

#### Option 3: Noxplayer

1. Install noxplayer (https://www.bignox.com/)
1. Run and enable root in settings
1. Log in with Google account
1. Install Whatsapp
1. Restore backup
1. Open Amaze file manager
1. Go to `/data/data/com.whatsapp/files/`
1. Copy the file named `key` (the file is created after the "Restoring messages" phase is done)
1. Paste file in `/mnt/shared/`
1. File will be available on host in `/Users/<user>/Library/Application Support/NoxAppPlayer/Nox_share/` (On Mac)
1. Close noxplayer and reactivate WhatsApp on phone


### How to copy WhatsApp folder from phone if it contains to many files

1. Install RCX in on you phone https://x0b.github.io/
1. Allow file system accees permissions
1. Install a webdav server (for ex. https://schimera.com/products/webdav-nav-server/)
1. Add the webdav server as a remote in RCX
1. Browse to a folder on the webdav remote in RCX
1. Tap (+) and Upload Files
1. Select the WhatsApp folder in `Android/media/com.whatsapp/`
1. (Optional) Enable stay Stay awake from Developer options