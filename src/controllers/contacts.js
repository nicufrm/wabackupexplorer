import { contacts } from '../models/contacts.js';

export const contactsPage = (req, res) => {
  res.status(200).json({
    contacts: contacts.list,
    loading: contacts.loading,
    error: contacts.error
  });
};
