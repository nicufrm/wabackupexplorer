export const TYPE = {
  TEXT: 0,
  IMAGE: 1,
  AUDIO: 2,
  VIDEO: 3,
  CONTACT: 4,
  LOCATION: 5,
  CALL: 8,
  FILE: 9,
  MISSED_CALL: 10,
  GIF: 13,
  DELETED: 15,
  LIVE_LOCATION: 16,
  STICKER: 20,
};

// export const TYPE = Object.keys(TYPE_INT).reduce((acc, key) => {
//   acc[key] = `${TYPE_INT[key]}`;
//   return acc;
// }, {});

export const ACTION_TYPE = {
  CHANGE_STATUS: 11,
  CHANGE_GROUP_ICON: 6,
  ADDED_YOU: 12,
  CODE_CHANGED: 18,
  MESSAGES_ARE_ENCRYPTED: 67,
};

export const STATUS = {
  READ: 0,
  SERVER: 4,
  DEVICE: 5,
  CONTROL: 6,
};
