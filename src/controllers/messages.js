import Model from '../models/model.js';

const messagesModel = new Model(`
  message
  LEFT JOIN message_media ON message_media.message_row_id = message._id
  LEFT JOIN message_thumbnail ON message_thumbnail.message_row_id = message._id
  LEFT JOIN jid ON jid._id = message.sender_jid_row_id
  LEFT JOIN message_system ON message_system.message_row_id = message._id
  LEFT JOIN message_quoted ON message_quoted.message_row_id = message._id
  LEFT JOIN message_quoted_media ON message_quoted_media.message_row_id = message._id
  LEFT JOIN jid AS jid_quoted ON jid_quoted._id = message_quoted.sender_jid_row_id

`);

export const messagesPage = (req, res) => {
  const data = messagesModel.all(`
    message._id,
    message.chat_row_id,
    message.from_me,
    message.key_id,
    message.sender_jid_row_id,
    message.status,
    message.broadcast,
    message.recipient_count,
    message.participant_hash,
    message.origination_flags,
    message.origin,
    message.timestamp,
    message.received_timestamp,
    message.receipt_server_timestamp,
    message.message_type,
    message.text_data,
    message.starred,
    message.lookup_tables,
    message.sort_id,
    message.message_add_on_flags,

    message_media.file_path,
    message_media.width,
    message_media.height,

    message_thumbnail.thumbnail AS thumbnail,

    message_quoted.from_me AS quoted_from_me,
    message_quoted.timestamp AS quoted_timestamp,
    message_quoted.message_type AS quoted_message_type,
    message_quoted.text_data AS quoted_text_data,
    message_quoted.key_id AS quoted_key_id,

    message_quoted_media.file_path AS quoted_file_path,
    message_quoted_media.width AS quoted_width,
    message_quoted_media.height AS quoted_height,
    message_quoted_media.thumbnail AS quoted_thumbnail,

    jid_quoted.user as quoted_user,

    message_system.action_type,

    jid.user

  `, `
    WHERE message.chat_row_id='${req.params.id}'
    ORDER BY message._id DESC
    LIMIT ${req.params.start}, ${req.params.count}
  `);

  const processedData = data.map(line => {
    const newLine = {};
    if (line.quoted_message_type !== null) {
      newLine.quoted = {};
    }
    Object.keys(line).forEach((prop) => {
      if (prop.startsWith('quoted_')) {
        if (newLine.quoted) {
          const newProp = prop.replace('quoted_', '');
          newLine.quoted[newProp] = line[prop];
        }
      } else {
        newLine[prop] = line[prop];
      }
    });

    if (newLine?.quoted?.thumbnail) {
      newLine.quoted.thumbnail = newLine?.quoted?.thumbnail.toString('base64');
    }

    if (newLine?.thumbnail) {
      newLine.thumbnail = newLine?.thumbnail.toString('base64');
    }

    return newLine;
  });
  res.status(200).json({ messages: processedData });

  // thumb_image is JPEG with extra header pic starts at 0x1B
};
