/* eslint-disable no-console */
const fs = require('fs');
const path = require('path');
const createLazyDecipheriv = require('@transcend-io/lazydecipheriv');
const inflate = require('zlib').createInflate();

const keyfile = path.join(__dirname, 'Key/key');
const c12file = path.join(__dirname, 'Files/WhatsApp/Databases/msgstore.db.crypt14');
const sqlfile = path.join(__dirname, 'Files/msgstore.db');

// // crypt12
// const CRYPT_SERVERSALT_OFFSET = 3;
// const CRYPT_IV_OFFSET = 51;
// const CRYPT_DATA_OFFSET = 67;
// const CRYPT_FOOTER_SIZE = 20;

// crypt14  (from https://github.com/andreas-mausch/whatsapp-viewer/issues/120)
const CRYPT_SERVERSALT_OFFSET = 15;
const CRYPT_IV_OFFSET = 67;
const CRYPT_DATA_OFFSET = 190;
const CRYPT_FOOTER_SIZE = 0;

const keyData = fs.readFileSync(keyfile);

const c12stats = fs.statSync(c12file);
const c12SizeInBytes = c12stats.size;

const c12fileDesc = fs.openSync(c12file, 'r');
const c12Data = Buffer.alloc(CRYPT_DATA_OFFSET);
const tag = Buffer.alloc(16);
fs.readSync(c12fileDesc, c12Data, 0, CRYPT_DATA_OFFSET, 0);
fs.readSync(c12fileDesc, tag, 0, 16, c12SizeInBytes - 36);
fs.closeSync(c12fileDesc);

const t1 = keyData.slice(30, 30 + 32);
const key = keyData.slice(126, 126 + 32);
const t2 = c12Data.slice(CRYPT_SERVERSALT_OFFSET, CRYPT_SERVERSALT_OFFSET + 32);
const iv = c12Data.slice(CRYPT_IV_OFFSET, CRYPT_IV_OFFSET + 16);

if (t1.equals(t2)) {
  console.log('Key matches encrypted database');
  console.log('Decrypting...');

  const algorithm = 'aes-256-gcm';
  const decipher = createLazyDecipheriv(algorithm, key, iv);
  // decipher.setAuthTag(tag)
  const input = fs.createReadStream(c12file, {
    start: CRYPT_DATA_OFFSET,
    end: c12SizeInBytes - CRYPT_FOOTER_SIZE - 1,
  });
  const output = fs.createWriteStream(sqlfile);
  input.pipe(decipher).pipe(inflate).pipe(output);

  output.on('finish', () => console.log('Done!'));
} else {
  console.log('Key doesn\'t match the encrypted database');
  console.log(t1);
  console.log(t2);
}
