import Database from 'better-sqlite3';
import { sqlFile } from '../settings.js';

export const db = new Database(sqlFile, { readonly: true, fileMustExist: true });
