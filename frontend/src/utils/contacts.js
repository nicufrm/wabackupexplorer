export const match = (searchNumber, contacts) => {
  if (searchNumber === null) return null;
  let index;
  index = contacts
    .findIndex(contact => contact.phones.includes(searchNumber));
  if (index !== -1) return index;
  index = contacts
    .findIndex(contact => contact.phones.findIndex(phone => searchNumber.endsWith(phone)));
  if (index !== -1) return index;
  return null;
};
