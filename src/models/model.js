import { db } from './db.js';

class Model {
  constructor(table) {
    this.table = table;
    this.db = db;
  }

  all(columns, clause) {
    let query = `SELECT ${columns} FROM ${this.table}`;
    if (clause) query += ` ${clause}`;
    const stmt = db.prepare(query);
    // stmt.raw(true);
    return stmt.all();
  }
}

export default Model;
